import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: `football`,
    loadChildren: () =>
      import('./modules/football/football.module').then(m => m.FootballModule)
    // loadChildren:'./modules/home/home.module#HomeModule'
  },
  {
    path: "",
    redirectTo: `football`,
    pathMatch: "full"
  },
  {
    path: "**",
    redirectTo: `football`,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
