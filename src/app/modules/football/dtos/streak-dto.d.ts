export interface StreakDto {
    id_team: number;
    id_player: number;
    gameweek: string;
    points: number;
    color: number;
}