import { Streak } from "../models/streak.model";

export interface PlayerDto {
    picture: string;
    name: string;
    position: string;
    id_team: number;
    id_comunio: number;
    value: number;
    points: number;
    avg: number;
    status: number;
    id_competition: number;
    ts_pic: number;
    prev_value: number;
    last_modified: string;
    id_uc: number;
    clause: number;
    streak: { [id: string]: Streak };
    shield: number;
    fav: number;
    match_info: { is_home: boolean, rival_team_id: number }[];
}