import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PlayerDto } from '../../dtos/player-dto';
import { TeamDto } from '../../dtos/team-dto';

@Injectable({
  providedIn: 'root'
})
export class LaLigaDataRequestService {

  constructor(
    private http: HttpClient
  ) { }


  getLaLigaDataTeams(): Observable<TeamDto[]> {
    return this.http.get<TeamDto[]>("http://localhost:3000/teams");
  }
  getLaLigaDataPlayers(): Observable<PlayerDto[]> {
    return this.http.get<PlayerDto[]>("http://localhost:3000/players");
  }
}
