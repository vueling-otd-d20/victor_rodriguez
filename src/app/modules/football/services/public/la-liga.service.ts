import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Player } from '../../models/player.model';
import { Team } from '../../models/team.model';
import { LaLigaDataRequestService } from '../private/la-liga-data-request.service';

@Injectable({
  providedIn: 'root'
})
export class LaLigaService {

  constructor(
    public laLigaDataRequestService: LaLigaDataRequestService) { }

  getLaLigaDataTeams(): Observable<Team[]> {
    return this.laLigaDataRequestService.getLaLigaDataTeams().pipe(
      map(teams => teams.map(team => new Team().FromDTO(team))))
  }

  getLaLigaDataPlayers(): Observable<Player[]> {
    return this.laLigaDataRequestService.getLaLigaDataPlayers().pipe(
      map(players => players.map(player => new Player().FromDTO(player))))
  }
}
