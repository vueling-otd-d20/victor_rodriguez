import { StreakDto } from "../dtos/streak-dto";

export class Streak {
    id_team: number;
    id_player: number;
    gameweek: string;
    points: number;
    color: number;

    ToDTO(): StreakDto {
        const dto: StreakDto = {
            id_team: this.id_team,
            id_player: this.id_player,
            gameweek: this.gameweek,
            points: this.points,
            color: this.color
        };
        return dto;
    }

    FromDTO(from: StreakDto): Streak {
        this.id_team = from.id_team;
        this.id_player = from.id_player;
        this.gameweek = from.gameweek;
        this.points = from.points;
        this.color = from.color;
        return this;
    }
}