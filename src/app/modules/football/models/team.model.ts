import { TeamDto } from "../dtos/team-dto";

export class Team {
    id: number;
    name: string;

    ToDTO(): TeamDto {
        const dto: TeamDto = {
            id: this.id,
            name: this.name
        };
        return dto;
    }

    FromDTO(from: TeamDto): Team {
        this.id = from.id;
        this.name = from.name;
        return this;
    }
}