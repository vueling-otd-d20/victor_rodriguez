import { PlayerDto } from "../dtos/player-dto";
import { TeamDto } from "../dtos/team-dto";
import { Streak } from "./streak.model";

export class Player {
    picture: string;
    name: string;
    position: string;
    id_team: number;
    id_comunio: number;
    value: number;
    points: number;
    avg: number;
    status: number;
    id_competition: number;
    ts_pic: number;
    prev_value: number;
    last_modified: Date;
    id_uc: number;
    clause: number;
    streak: { [id: string]: Streak };
    shield: number;
    fav: number;
    match_info: { is_home: boolean, rival_team_id: number }[];

    ToDTO(): PlayerDto {
        const dto: PlayerDto = {
            picture: this.picture,
            name: this.name,
            position: this.position,
            id_team: this.id_team,
            id_comunio: this.id_comunio,
            value: this.value,
            points: this.points,
            avg: this.avg,
            status: this.status,
            id_competition: this.id_competition,
            ts_pic: this.ts_pic,
            prev_value: this.prev_value,
            last_modified: this.last_modified ? this.last_modified.toLocaleString() : null,
            id_uc: this.id_uc,
            clause: this.clause,
            streak: this.streak,
            shield: this.shield,
            fav: this.fav,
            match_info: this.match_info
        };
        return dto;
    }

    FromDTO(from: PlayerDto): Player {
        this.picture = from.picture;
        this.name = from.name;
        this.position = from.position;
        this.id_team = from.id_team;
        this.id_comunio = from.id_comunio;
        this.value = from.value;
        this.points = from.points;
        this.avg = from.avg;
        this.status = from.status;
        this.id_competition = from.id_competition;
        this.ts_pic = from.ts_pic;
        this.prev_value = from.prev_value;
        this.last_modified = from.last_modified ? new Date(from.last_modified) : null;
        this.id_uc = from.id_uc;
        this.clause = from.clause;
        this.streak = from.streak;
        this.shield = from.shield;
        this.fav = from.fav;
        this.match_info = from.match_info;
        return this;
    }
}