import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Player } from '../../models/player.model';

@Component({
  selector: 'xxx-player-info',
  templateUrl: './player-info.component.html',
  styleUrls: ['./player-info.component.css']
})
export class PlayerInfoComponent implements OnInit {

  player: Player;

  constructor() { }

  ngOnInit(): void {
  }

}
