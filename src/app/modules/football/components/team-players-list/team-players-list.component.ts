import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Player } from '../../models/player.model';
import { LaLigaService } from '../../services/public/la-liga.service';
import { PlayerInfoComponent } from '../player-info/player-info.component';

@Component({
  selector: 'xxx-team-players-list',
  templateUrl: './team-players-list.component.html',
  styleUrls: ['./team-players-list.component.scss']
})
export class TeamPlayersListComponent implements OnInit {

  teamID: number;
  teamName: string;

  players: Player[] = [];
  constructor(
    private laLigaService: LaLigaService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.teamID = Number(this.route.snapshot.paramMap.get('id'));
    this.teamName = this.route.snapshot.queryParamMap.get('teamName');
  }

  ngOnInit(): void {
    this.laLigaService.getLaLigaDataPlayers().subscribe(result => {
      result.forEach(player => {
        if (player.id_team == this.teamID) {
          this.players.push(player);
        }
      });
    })
  }

  openModal(player) {
    const dialogRef = this.dialog.open(PlayerInfoComponent, { height: '800px', width: '800px' });
    dialogRef.componentInstance.player = player;

    dialogRef.afterClosed().subscribe(result => {

    });
  }

}
