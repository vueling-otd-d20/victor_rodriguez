import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Team } from '../../models/team.model';
import { LaLigaService } from '../../services/public/la-liga.service';

@Component({
  selector: 'xxx-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {

  displayedColumns: string[] = ['name'];
  teams: Team[];

  constructor(
    private laLigaService: LaLigaService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.laLigaService.getLaLigaDataTeams().subscribe(result => {
      if (result) {
        this.teams = result;
      }
    })
  }

  onRowClick(row) {
    this.router.navigate(["football", row.id], { queryParams: { teamName: row.name } });
  }

}
