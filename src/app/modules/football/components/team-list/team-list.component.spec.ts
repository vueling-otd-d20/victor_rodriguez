import { HttpClientModule } from '@angular/common/http';
import { ChangeDetectionStrategy } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Team } from '../../models/team.model';
import { LaLigaDataRequestService } from '../../services/private/la-liga-data-request.service';
import { TeamListComponent } from './team-list.component';
import { By } from '@angular/platform-browser';

describe('TeamListComponent', () => {
  let component: TeamListComponent;
  let fixture: ComponentFixture<TeamListComponent>;

  let mockLaligaService: LaLigaDataRequestService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamListComponent],
      imports: [RouterModule.forRoot([]), HttpClientModule, MatTableModule]
    })
      .overrideComponent(TeamListComponent, {
        set: { changeDetection: ChangeDetectionStrategy.Default }
      })
      .compileComponents();

    mockLaligaService = TestBed.inject(LaLigaDataRequestService);
    spyOn(mockLaligaService, 'getLaLigaDataTeams').and.returnValue(of([{ id: 1, name: 'Barcelona' }, { id: 2, name: 'Madrid' }]));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have teams', () => {
    expect(component.teams).toBeDefined();
  });

  it('should have a grid with teams', () => {
    const result = fixture.debugElement.query(By.css('.mat-table'));
    expect(result.nativeElement.innerText).toContain('Equipo\nBarcelona\nMadrid');
  });
});
