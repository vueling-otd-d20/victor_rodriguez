import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamListComponent } from './components/team-list/team-list.component';
import { TeamPlayersListComponent } from './components/team-players-list/team-players-list.component';


const routes: Routes = [{
  path: ``,
  component: TeamListComponent
},
{
  path: ':id',
  component: TeamPlayersListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FootballRoutingModule { }
